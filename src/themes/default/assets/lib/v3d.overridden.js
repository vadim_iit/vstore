console.log('v3d', v3d);

v3d.App.prototype._saveAnimations = function (app) {
  app.allAnimations = [];
  if(!app.actions) return;
  for(var i=0; i<app.actions.length; i++) {
    var animationObject = {};
    animationObject.auto = false;
    animationObject.clip = app.actions[i]._clip;
    animationObject.loop = app.actions[i].loop;
    animationObject.repetitions = app.actions[i].repetitions;
    animationObject.startAt = 0;
    app.allAnimations.push(animationObject);
  }
}

v3d.App.prototype._updateMixer = function (animations, app) {
    var a = animations;
    var n = app.scene;
    var i = app;
    
    if (i.actions = [], a && a.length) {
        if (i.mixer === null) {
            i.allAnimations = animations;
        } else {
            // app.mixer.uncacheRoot(app.mixer._root);          
            app.mixer = null;
            delete app.mixer;
            for (var o = 0; o < a.length; o++) {
                for (var j = 0; j < i.allAnimations.length; j++) {
                  if (a[o].clip.name === i.allAnimations[j].clip.name) {
                    i.allAnimations[j] = a[o];
                  }
                }
              }
    
        }
        
      i.mixer = new THREE.AnimationMixer(n);
      a = i.allAnimations;

      for (var o = 0; o < a.length; o++) {
        var s = a[o];
        if (n.getObjectById(s.nodeId)) {
          console.log('n.getObjectById(s.node_id)', n.getObjectById(s.node_id));
          var l = i.mixer.clipAction(s.clip);
          l.setLoop(s.loop, s.repetitions), l.startAt(s.startAt), l.clampWhenFinished = !0, s.auto ? l.play() : (l.stop(), l.paused = !0), i.actions.push(l)
          console.log('i.actions', i.actions);
        }
      }
    }
    i.initPostprocessing(), i.onResize && i.onResize(), i.scene.updateMatrixWorld();
  }



