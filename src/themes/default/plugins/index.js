// Add your own plugins in this folder with Vue.use()
import Vue from 'vue'
import * as THREE from '../assets/lib/three.js'
// import * as v3dmodule from '../assets/lib/v3d.module.js'
import VueProgressBar from 'vue-progressbar'

Vue.use(VueProgressBar)
Object.defineProperty(Vue.prototype, '$THREE', { value: THREE })
// Object.defineProperty(Vue.prototype, '$v3dmodule', { value: v3dmodule })
